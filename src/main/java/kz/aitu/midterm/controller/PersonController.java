package kz.aitu.midterm.controller;

import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PersonController{

    final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/")
    public List<Person> getPerson() {
        return personService.getAll();
    }

    @PutMapping("/api/v2/users/{id}")
    public String updateById(@PathVariable Long id, @Validated @RequestBody Person fond) {
        personService.update(fond);
        return "Person is updated!";
    }
    @DeleteMapping("/api/v2/users/{id}")
    public String deleteById(@PathVariable long id) {
        personService.deleteById(id);
        return "Person is deleted!";
    }

}