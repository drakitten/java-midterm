package kz.aitu.midterm.service;

import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll() {
        return (List<Person>) personRepository.findAll();
    }

    public void update(Person item) {
        personRepository.save(item);
    }

    public void deleteById(long id) {
        personRepository.deleteById(id);
    }
}
