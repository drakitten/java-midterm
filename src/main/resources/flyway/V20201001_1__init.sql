
CREATE TABLE person (
    id serial NOT NULL,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    city VARCHAR(255),
    phone VARCHAR(11),
    telegram VARCHAR(255),
    PRIMARY KEY (id)
);